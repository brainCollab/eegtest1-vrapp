using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlaceOnControllerClic : MonoBehaviour
{
    private UnityEngine.XR.InputDevice controllerRight;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        var gameControllers = new List<UnityEngine.XR.InputDevice>();
        UnityEngine.XR.InputDevices.GetDevicesWithCharacteristics(UnityEngine.XR.InputDeviceCharacteristics.Right, gameControllers);
        if(gameControllers.Count > 0)
        {  
            controllerRight = gameControllers[0];
            if(controllerRight.TryGetFeatureValue(UnityEngine.XR.CommonUsages.triggerButton, out bool triggerButton) && triggerButton)
            {
                transform.position = controllerRight.TryGetFeatureValue(UnityEngine.XR.CommonUsages.devicePosition, out Vector3 position) ? position : Vector3.zero;
            }
        }
    }
}