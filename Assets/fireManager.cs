using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ParticleSystem))]
public class fireManager : MonoBehaviour
{
    private ParticleSystem fire;

    public float intensity = 1.0f;

    public float speedModifier_min = 0;
    public float speedModifier_max = 2;

    public Gradient colorOverLifeTime_min;
    public Gradient colorOverLifeTime_max;
    
    [ColorUsage(true, true)]
    public Color color_min;
    
    [ColorUsage(true, true)]
    public Color color_max;

    public float Intensity { get => intensity; set => intensity = value; }

    // Start is called before the first frame update
    void Start()
    {
        fire = GetComponent<ParticleSystem>();
    }

    // Update is called once per frame
    void Update()
    {
        var vel = fire.velocityOverLifetime;
        vel.speedModifier = new ParticleSystem.MinMaxCurve(Mathf.Lerp(speedModifier_min, speedModifier_max, Intensity));
        
        var col = fire.colorOverLifetime;
        col.color = BlendGradients(colorOverLifeTime_min, colorOverLifeTime_max, Intensity);

        ParticleSystemRenderer renderer = fire.GetComponent<ParticleSystemRenderer>();
        renderer.material.SetColor("_EmissionColor", Color.Lerp(color_min, color_max, Intensity));
    }

    private Gradient BlendGradients(Gradient colorOverLifeTime_min, Gradient colorOverLifeTime_max, float intensity)
    {
        Gradient grad = new Gradient();
        var colorKeys = new GradientColorKey[colorOverLifeTime_max.colorKeys.Length];
        var alphaKeys = new GradientAlphaKey[colorOverLifeTime_max.alphaKeys.Length];
        for (int i = 0; i < colorKeys.Length; i++)
        {
            colorKeys[i].color = Color.Lerp(colorOverLifeTime_min.colorKeys[i].color, colorOverLifeTime_max.colorKeys[i].color, intensity);
            colorKeys[i].time = colorOverLifeTime_min.colorKeys[i].time;
        }
        for (int i = 0; i < alphaKeys.Length; i++)
        {
            alphaKeys[i].alpha = Mathf.Lerp(colorOverLifeTime_min.alphaKeys[i].alpha, colorOverLifeTime_max.alphaKeys[i].alpha, intensity);
            alphaKeys[i].time = colorOverLifeTime_min.alphaKeys[i].time;
        }
        grad.SetKeys(colorKeys, alphaKeys);
        return grad;
    }
}
